package com.springmongotemplatecrud.springmongotemplatecrud.service;

import com.springmongotemplatecrud.springmongotemplatecrud.model.Employee;
import com.springmongotemplatecrud.springmongotemplatecrud.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    public Employee save(Employee emp) {
        emp.setJoiningDate(new Date( ));
        return employeeRepository.save(emp);
    }

    public List<Employee> getAll() {
        return employeeRepository.find();
    }

    public Employee update(Employee emp) {
        return employeeRepository.save(emp);
    }

    public long delete(Employee emp) {
        return employeeRepository.delete(emp);
    }

    public List<Employee> getBySalary(int salary) {
        return employeeRepository.findBySalary(salary);
    }

    public List<Employee> getByFirstName(String firstName) {
        return employeeRepository.findByFirstName(firstName);
    }
}
